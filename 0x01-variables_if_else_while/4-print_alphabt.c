#include <stdio.h>

/**
 * main - prints the alphabet in lowercase,
 * followed by a new line, except q and e
 * Return: Always 0 (Success)
 */
int main(void)
{
	char he = 'a';

	while (he <= 'z')
	{
		if (he != 'e' && he != 'q')
		{
			putchar(he);
		}
			he++;
	}
	putchar('\n');
	return (0);
}
