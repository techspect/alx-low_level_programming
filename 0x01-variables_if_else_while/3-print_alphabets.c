#include <stdio.h>

/**
 * main - prints the alphabet in lowercase, and then in uppercase,
 * followed by a new line
 * Return: Always 0 (Success)
 */
int main(void)
{
	int he;

	for (he = 'a'; he <= 'z'; he++)
		putchar(he);
	for (he = 'A'; he <= 'Z'; he++)
		putchar(he);
	putchar('\n');
	return (0);
}

