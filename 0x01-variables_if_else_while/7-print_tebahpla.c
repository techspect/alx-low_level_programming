#include <stdio.h>

/**
 * main - prints the lowercase alphabet in reverse,
 * followed by a new line
 * Return: Always 0 (Success)
 */
int main(void)
{
	char he;

	for (he = 'z'; he >= 'a'; he--)
	{
		putchar(he);
	}
	putchar('\n');
	return (0);
}
