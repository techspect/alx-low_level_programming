#include <stdio.h>

/**
 * main - prints the alphabet in lowercase,
 * followed by a new line
 * Return: Always 0 (Success)
 */
int main(void)
{
	char he;

	for (he = 'a'; he <= 'z'; he++)
	{
		putchar(he);
	}
	putchar('\n');
	return (0);
}
