C - Static libraries
In this project, I learned what static libraries are and practiced creating and using them with ar, ranlib, and nm.

Tests: Folder of test files. Provided by Holberton School.

0. A library is not a luxury but one of the necessities of life

1. Without libraries what have we? We have no past and no future
